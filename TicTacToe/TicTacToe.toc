## Interface: 90200
## Title: TicTacToe
## Notes: TicTacToe minigame - challenge someone using /tictactoe nick, or join lobby by right clicking minimap icon.
## Version: 2.8
## Author: Xiledria
## SavedVariables: TicTacToeSV
Core.lua
UIScripts.lua
Init.lua
Bot.lua
Lobby.lua
UI.xml